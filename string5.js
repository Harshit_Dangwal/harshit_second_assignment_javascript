function getString(testString){
    if(getString){
        return testString.join(" ");
    }else{
        return ""
    }
}

//console.log(getString(["the", "quick", "brown", "fox"]))

module.exports = getString;
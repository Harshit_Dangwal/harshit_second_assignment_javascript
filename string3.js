function getMonthName(testString) {
    if (testString) {
        const month = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        const d= new Date(testString); 
        if (!month[d.getMonth()]){
            return ''
        }else{
            return month[d.getMonth()];
        }
        
    }else{
        return "";
    }
}
console.log(getMonthName("12/1/2020"));

module.exports=getMonthName;
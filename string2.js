function ipArray(testString) {
    if (testString) {
        let result =[]
        for (value of testString.split('.')) {
            if (value >= 0 && value <= 255) {
                result.push(parseInt(value));
            } else {
                return [];
            }
        }
        return result;
    } else {
        return undefined;
    }
}


console.log(ipArray())

module.exports = ipArray;
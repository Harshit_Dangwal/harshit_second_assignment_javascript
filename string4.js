function getFullName(testString) {
    if (testString) {

        if (testString.first_name) {
            firstName="";
            firstName = testString.first_name[0].toUpperCase();
            for (let index = 1; index < testString.first_name.length; index++) {
                firstName += testString.first_name[index].toLowerCase()
            }
        }
        if (testString.last_name) {
            lastName =''
            lastName = testString.last_name[0].toUpperCase();
            for (let index = 1; index < testString.last_name.length; index++) {
                lastName += testString.last_name[index].toLowerCase()

            }
        }
        if (testString.middle_name) {
            middleName = ''
            middleName = testString.middle_name[0].toUpperCase();
            for (let index = 1; index < testString.middle_name.length; index++) {
                middleName += testString.middle_name[index].toLowerCase()
            }
        }

        if (testString.middle_name) {
            return firstName + " " + middleName + " " + lastName;
        }
        if (testString.lastName) {
            return firstName + ' ' + lastName;
        }else{
            return firstName
        }
    } else {
        return ""
    }

}

//console.log(getFullName({ "first_name": "JoHN" }));

module.exports = getFullName;
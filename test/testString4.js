const getFullName = require("../string4");


console.log(getFullName({"first_name": "JoHN", "last_name": "SMith"}))
// John Smith
console.log(getFullName({"first_name": "JoHN", "middle_name": "doe", "last_name": "SMith"}))
// John Doe Smith
console.log(getFullName(({first_name:"Harshit"}))
// Harshit
function toNumeric(testString) {
    if (testString) {
        if (testString[0] === '$' || testString[0] === '-' || testString[0] === '+') {
            return parseFloat((testString).replace("$", ''));
        }
        else {
            return 0;
        }
    }else{
        return undefined;
    }
}

//console.log(toNumeric('+$56.76'));

module.exports = toNumeric;

